package com.twuc.webApp.domain.composite;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Location {
    @Column(nullable = false,length = 128)
    private String city;
    @Column(nullable = false,length = 128)
    private String street;

    public Location(String city, String street) {
        this.city = city;
        this.street = street;
    }

    public Location() {
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }
}
