package com.twuc.webApp.domain.composite;

import javax.persistence.*;

@Entity
public class UserProfile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "city", column = @Column(name = "address_city")),
            @AttributeOverride(name = "street", column = @Column(name = "address_street"))
    })
    private Location location;

    public Long getId() {
        return id;
    }

    public Location getLocation() {
        return location;
    }

    public UserProfile(Location location) {
        this.location = location;
    }

    public UserProfile() {
    }
}
