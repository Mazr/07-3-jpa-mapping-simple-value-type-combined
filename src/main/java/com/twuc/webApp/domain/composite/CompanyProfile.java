package com.twuc.webApp.domain.composite;

import javax.persistence.*;

@Entity
public class CompanyProfile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Embedded
    private Location location;

    public Long getId() {
        return id;
    }

    public CompanyProfile(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

    public CompanyProfile() {
    }

}
