package com.twuc.webApp.domain.composite;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class SimpleMappingAndValueTypeTest extends JpaTestBase {
    @Autowired
    private UserProfileRepository userProfileRepository;
    @Autowired
    private CompanyProfileRepository companyProfileRepository;
    @Test
    void should_test_what_sql_I_get_when_create_user_profile() {
        ClosureValue<Long> expectedId = new ClosureValue<>();
        flushAndClear(entityManager -> {
            final UserProfile userProfile = userProfileRepository.save(
                    new UserProfile(new Location("xian","zhuquedajie")));
            expectedId.setValue(userProfile.getId());
        });
        run(entityManager -> {
            final UserProfile userProfile = userProfileRepository
                    .findById(expectedId.getValue())
                    .orElseThrow(RuntimeException::new);
            assertEquals("xian",userProfile.getLocation().getCity());
            assertEquals("zhuquedajie",userProfile.getLocation().getStreet());
        });
    }
    @Test
    void should_test_what_sql_I_get_when_create_company_profile() {
        ClosureValue<Long> expectedId = new ClosureValue<>();
        flushAndClear(entityManager -> {
            final CompanyProfile companyProfile = companyProfileRepository.save(
                    new CompanyProfile(new Location("beijing","xingfudajie")));
            expectedId.setValue(companyProfile.getId());
        });
        run(entityManager -> {
            final CompanyProfile companyProfile = companyProfileRepository
                    .findById(expectedId.getValue())
                    .orElseThrow(RuntimeException::new);
            assertEquals("beijing",companyProfile.getLocation().getCity());
            assertEquals("xingfudajie",companyProfile.getLocation().getStreet());
        });
    }

}
